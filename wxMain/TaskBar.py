import wx
import os
from kcdatabase.kcdatabase import KCDatabase
#from wxMain.Info import Info
from datetime import datetime

TRAY_TOOLTIP = 'KC-Monitor'
TRAY_ICON = 'img\\icon.ico'

class TaskBarIcon(wx.TaskBarIcon):

    def __init__(self):
        super(TaskBarIcon, self).__init__()

        exename = r'KC-Monitor.exe'
        if os.path.exists(exename):
            loc = wx.IconLocation(exename, 0)
            self.SetIcon(wx.IconFromLocation(loc))
        else:
            self.set_icon(TRAY_ICON)

        self.Bind(wx.EVT_TASKBAR_LEFT_DCLICK, self.on_check_status)
        self.last_date = None

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_check_status, self.timer)
        self.timer.Start(60000)
        #self.CheckNow(True)

    def create_menu_item(self, menu, label, func):
        item = wx.MenuItem(menu, -1, label)
        menu.Bind(wx.EVT_MENU, func, id=item.GetId())
        menu.AppendItem(item)
        return item

    def CreatePopupMenu(self):
        menu = wx.Menu()
        self.create_menu_item(menu, 'Stan dnia', self.on_check_status)
        self.create_menu_item(menu, 'Konfiguracja', self.on_show_config)
        menu.AppendSeparator()
        self.create_menu_item(menu, 'Zakoncz', self.on_exit)
        return menu

    def set_icon(self, path):
        icon = wx.IconFromBitmap(wx.Bitmap(path))
        self.SetIcon(icon, TRAY_TOOLTIP)

    def on_show_config(self, event):
        self.ShowConfig()

    def on_check_status(self, event):
        self.CheckStatus()

    def on_exit(self, event):
        wx.CallAfter(self.Destroy)

    def CheckStatus(self):
        pass
        """
        print datetime.now()
        t = tablica()
        ogloszenia = t.search('navington+cadet')
        if ogloszenia:
            last_date = ogloszenia[0]['date']
            for o in ogloszenia:
              if o['date'] > last_date:
                  last_date = o['date']
            s = str(last_date)
            if not self.last_date:
                self.last_date = last_date
                s = 'Ostatnie ogloszenie dodano: '  + str(self.last_date)
            else:
                if self.last_date <> last_date:
                    self.last_date = last_date
                    s = 'Ostatnie ogloszenie dodano: '  + str(self.last_date)
                else:
                    if ShowMsg:
                        s = 'Brak nowych ogloszen.'
                    else:
                        s = ''
            if s <> '':
                msg = wx.MessageDialog(None, s, 'tablica.pl', wx.OK|wx.STAY_ON_TOP)
                msg.ShowModal()
        else:
            s = 'Brak danych'
            msg = wx.MessageDialog(None, s, 'Ostatnio dodane', wx.OK|wx.STAY_ON_TOP)
            msg.ShowModal()
        """