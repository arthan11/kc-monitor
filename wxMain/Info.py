import wx
from wx.lib.hyperlink import HyperLinkCtrl
from cStringIO import StringIO
import os, urllib
from tablica.tablica import tablica

class Info(wx.Frame):
    
    def __init__(self):
        super(Info, self).__init__(None, title='tablica.pl', size=(500, 250))

        exename = r'Tablica.exe'
        if os.path.exists(exename):
            loc = wx.IconLocation(exename, 0)
            self.SetIcon(wx.IconFromLocation(loc))

        t = tablica()
        ogloszenia = t.search('navington+cadet')
        o = ogloszenia[0]

        pnl = wx.Panel(self)
        pnl.SetBackgroundColour('#ffffff')

        wx.StaticText(pnl, label=str(o['date']), pos=(25, 100))
        wx.StaticText(pnl, label=o['nazwa'].decode('utf-8'), pos=(25, 120))
        wx.StaticText(pnl, label=o['price'].decode('utf-8'), pos=(25, 140))
        if o['do_negocjacji']:
          wx.StaticText(pnl, label='Do negocjacji', pos=(25, 160))

        #wx.wxInitAllImageHandlers()
        fp = urllib.urlopen(o['img'])
        data = fp.read()
        fp.close()
        img = wx.ImageFromStream(StringIO(data))
        bmp = wx.BitmapFromImage(img)
        wx.StaticBitmap(pnl, -1, bmp, (25, 15))

        o['link'] = o['link'].replace('/m/','/')
        self.link = HyperLinkCtrl(parent=pnl, pos=(25, 180))
        self.link.AutoBrowse(False)
        self.link.SetURL(URL=o['link'])
        self.link.SetLabel(label='przejdz do strony')
        self.link.SetToolTipString(tip=o['link'])
        self.link.Bind(wx.EVT_LEFT_DOWN, self.OnLink)

        #cbtn = wx.Button(pnl, label='Zamknij', pos=(200, 140))
        #cbtn.Bind(wx.EVT_BUTTON, self.OnClose)

        self.Centre()
        self.Show()

    def OnLink(self, event):
        self.link.GotoURL(self.link.GetURL())
        self.Close()

