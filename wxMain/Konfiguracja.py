#Boa:Dialog:DlgConfig

import wx

def create(parent):
    return DlgConfig(parent)

[wxID_DLGCONFIG, wxID_DLGCONFIGCBAUTOSTART, 
 wxID_DLGCONFIGCBSPOSOBPOWIADAMIANIA, wxID_DLGCONFIGSBXPOWIADAMIANIE, 
 wxID_DLGCONFIGSPNPOWIADAMIAJCO, wxID_DLGCONFIGSPNSPRAWDZAJCO, 
 wxID_DLGCONFIGST1, wxID_DLGCONFIGST2, wxID_DLGCONFIGST3, wxID_DLGCONFIGST4, 
 wxID_DLGCONFIGST5, 
] = [wx.NewId() for _init_ctrls in range(11)]

class DlgConfig(wx.Dialog):
    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_DLGCONFIG, name=u'DlgConfig',
              parent=prnt, pos=wx.Point(530, 305), size=wx.Size(319, 191),
              style=wx.DEFAULT_DIALOG_STYLE, title=u'Konfiguracja')
        self.SetClientSize(wx.Size(311, 164))

        self.cbAutostart = wx.CheckBox(id=wxID_DLGCONFIGCBAUTOSTART,
              label=u'Uruchom przy starcie systemu', name=u'cbAutostart',
              parent=self, pos=wx.Point(8, 16), size=wx.Size(168, 13), style=0)
        self.cbAutostart.SetValue(True)

        self.spnSprawdzajCo = wx.SpinCtrl(id=wxID_DLGCONFIGSPNSPRAWDZAJCO,
              initial=0, max=10000, min=0, name=u'spnSprawdzajCo', parent=self,
              pos=wx.Point(128, 40), size=wx.Size(64, 21),
              style=wx.SP_ARROW_KEYS)

        self.st2 = wx.StaticText(id=wxID_DLGCONFIGST2, label=u'sekund',
              name=u'st2', parent=self, pos=wx.Point(200, 48), size=wx.Size(34,
              13), style=0)

        self.st1 = wx.StaticText(id=wxID_DLGCONFIGST1,
              label=u'Sprawdzanie kwoty co', name=u'st1', parent=self,
              pos=wx.Point(16, 48), size=wx.Size(107, 13), style=0)

        self.sbxPowiadamianie = wx.StaticBox(id=wxID_DLGCONFIGSBXPOWIADAMIANIE,
              label=u' Powiadamianie ', name=u'sbxPowiadamianie', parent=self,
              pos=wx.Point(8, 72), size=wx.Size(288, 80), style=0)

        self.st3 = wx.StaticText(id=wxID_DLGCONFIGST3, label=u'powiadamiaj co',
              name=u'st3', parent=self, pos=wx.Point(24, 96), size=wx.Size(73,
              13), style=0)

        self.spnPowiadamiajCo = wx.SpinCtrl(id=wxID_DLGCONFIGSPNPOWIADAMIAJCO,
              initial=0, max=10000, min=0, name=u'spnPowiadamiajCo',
              parent=self, pos=wx.Point(104, 88), size=wx.Size(72, 21),
              style=wx.SP_ARROW_KEYS)
        self.spnPowiadamiajCo.SetValue(1000)
        self.spnPowiadamiajCo.SetToolTipString(u'spinCtrl2')

        self.st4 = wx.StaticText(id=wxID_DLGCONFIGST4, label=u'z\u0142',
              name=u'st4', parent=self, pos=wx.Point(184, 96), size=wx.Size(8,
              13), style=0)

        self.st5 = wx.StaticText(id=wxID_DLGCONFIGST5,
              label=u'Spos\xf3b powiadamiania', name=u'st5', parent=self,
              pos=wx.Point(24, 128), size=wx.Size(108, 13), style=0)

        self.cbSposobPowiadamiania = wx.ComboBox(choices=[],
              id=wxID_DLGCONFIGCBSPOSOBPOWIADAMIANIA,
              name=u'cbSposobPowiadamiania', parent=self, pos=wx.Point(136,
              120), size=wx.Size(130, 21), style=0, value='comboBox1')

    def __init__(self, parent):
        self._init_ctrls(parent)
