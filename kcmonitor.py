import wx
from wxMain.TaskBar import TaskBarIcon

class MyApp(wx.PySimpleApp):
    def __init__(self):
        super(MyApp, self).__init__(redirect=True, filename='log.txt')

def main():
    app = MyApp()
    TaskBarIcon()
    app.MainLoop()

if __name__ == '__main__':
    main()