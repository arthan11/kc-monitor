#import cx_freeze module
from cx_Freeze import setup,Executable

"""
include is a list with the modules you are using in your program (built in modules and modules made by you)
if you use anydbm you should put a database alternative like dbhash or any database module you have
"""
includes = ["sys", "wx", "datetime", "time", "os", "decimal", "pyodbc"]

#script is the main script base Win32GUI is for windows
exe = Executable(
    script="kcmonitor.py",
    base = 'Win32GUI',
    targetName = "KC-Monitor.exe",
    compress = True,
    icon = "img/icon.ico"
    )

"""
    initScript = None,
    targetDir = r"dist",
    copyDependentFiles = True,
    appendScriptToExe = True,
    appendScriptToLibrary = False,

    )
"""


#version is the program version
setup(version="3.0",options={"build_exe":{"includes":includes}},executables = [exe])