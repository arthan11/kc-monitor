import pyodbc, datetime

class KCDatabase:
    def connect(self, DBFileName):
        # connect to db
        DRV = '{Microsoft Access Driver (*.mdb)}'
        #PWD = 'pw'
        self.con = pyodbc.connect('DRIVER={};DBQ={}'.format(DRV, DBFileName))
        self.cur = self.con.cursor()

    # run a query and get the results 
    #SQL = 'SELECT sum(HistoriaFin.ce_sb), sum(HistoriaFin.ce_sn) from DokFin, HistoriaFin where HistoriaFin.IdDokFin = DokFin.Id and DokFin.DataWystawienia = ?;'

    def get_salery_sum(self, DateFrom, DateTo):
        # suma sprzedazy brutto w  danych dniach
        SQL = 'SELECT sum(DokFin.wa_sbr) from DokFin where DokFin.DataWystawienia >= ? and DokFin.DataWystawienia <= ?'
        rows = self.cur.execute(SQL, (DateFrom, DateTo)).fetchall()
        if rows:
            return rows[0][0]
        else:
            return 0

if __name__ == "__main__":
    DBFileName = r'd:\PythonScripts\Aptana Projects\KC-Monitor\mdb\ABC_LUZINO.DAT'
    KCDB = KCDatabase()
    KCDB.connect(DBFileName)
    suma_03 = KCDB.get_salery_sum(datetime.date(2013, 03, 01), datetime.date(2013, 03, 31))
    suma_03_01 =  KCDB.get_salery_sum(datetime.date(2013, 03, 01), datetime.date(2013, 03, 01))
    print '%.2f' % suma_03
    print '%.2f' % suma_03_01



"""
# sprzedaz brutto danego dnia w rozbiciu godzinowym
SQL = 'SELECT godzina, sum(DokFin.wa_sbr) from DokFin where DokFin.DataWystawienia = ? group by godzina;'
rows = cur.execute(SQL, (datetime.date(2013, 04, 01))).fetchall()
cur.close()
con.close()

suma = 0
for godz, wart in rows:
    suma = suma + wart 
    print '%02d:00-%02d:00 %9.2f' % (godz, godz+1, wart)
print 'SUMA: %15.2f' % suma
"""

"""
# TOP20 - ilosci
SQL = '''SELECT Towary.Nazwa, sum(HistoriaFin.ilosc), sum(HistoriaFin.ilosc * HistoriaFin.ce_sb)
      from DokFin, HistoriaFin, Towary
      where HistoriaFin.IdDokFin = DokFin.Id and Towary.Id = HistoriaFin.IdTowaru and DokFin.DataWystawienia = ?
      group by Towary.Nazwa
      order by 2 desc, 1 asc
      '''
rows = cur.execute(SQL, (datetime.date(2013, 04, 01))).fetchall()
cur.close()
con.close()

for i in range(20):
  print '%2d. %s %7.3f %7.2f' % (i+1, rows[i][0].ljust(45), rows[i][1], rows[i][2])
"""
